/**
 * Gives the answer to the Euler problem #3
 * 
 * @author Marie Clemessy <marie.clemessy@gmail.com>
 * 
 */

package com.EulerAnswers;

import com.functions.Multiples;

public class Problem3 {
	
	public static void main(String[] args) {
		System.out.println("Je travaille");
		
		long variable = 600851475143L;
		long reponse = Multiples.getHighestPrimeFactor(variable);
		
		System.out.println("The highest prime factor of " + variable + " is " + reponse);
		System.out.println("Je ne travaille plus");
	}	

}
