/**
 * Gives the answer to the Euler problem #1
 * 
 * @author Marie Clemessy <marie.clemessy@gmail.com>
 * 
 */

package com.EulerAnswers;

import com.functions.Multiples;
import com.functions.Array;

public class Problem1 {

	public static void main(String[] args) {
		int borneSup = 1000;
		int answer = 0;
		System.out.println("Je travaille");
		Multiples ans = new Multiples(borneSup);
		int[] multiplesOfThree = ans.getMultiplesBelow(borneSup, 3);
		int[] multiplesOfFive = ans.getMultiplesBelow(borneSup, 5);
		int[] multiplesOfThreeAndFive = Array.MergeTables(multiplesOfThree, multiplesOfFive);
		answer = Array.SumOfAllElements(multiplesOfThreeAndFive);
		System.out.println("La somme des multiples de 3 ou 5 en dessous de " + borneSup + " est : " + answer );
		System.out.println("Je ne travaille plus");
	}

}
