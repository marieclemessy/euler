/**
 * Gives the answer to the Euler problem #2
 * 
 * @author Marie Clemessy <marie.clemessy@gmail.com>
 * 
 */

package com.EulerAnswers;

import com.functions.Fibonacci;
import com.functions.Array;

public class Problem2 {

		public static int borneSup = 4000000;
		
		public static void main(String[] args) {
			System.out.println("Je travaille");
			
			int[] fib = Fibonacci.FibonacciSuite(borneSup);
			long reponse = Array.SumOfPairElements(fib);
			
			System.out.println("La somme des entiers pairs dans la suite de Fibonacci n'exc�dant pas " + borneSup + " est : " + reponse);
			System.out.println("Je ne travaille plus");
		}				
}
