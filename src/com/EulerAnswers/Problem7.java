package com.EulerAnswers;

import com.functions.Multiples;

/**
 *
 * Gives the answer to the Euler problem #7
 *
 * @author Marie Clemessy <marie.clemessy@gmail.com>
 *
 */

public class Problem7 {


    /**
     * Search for the index-th prime number
     *
     * @param index The methods is looking for the prime number which index is index - 1
     *
     * @return Returns the index-th prime number
     */

    public static int searchPrimeOfIndex(int index){
        // Generates an array of the length index which will store the list of prime numbers
        int[] primeList = new int[index];

        // First prime number is 2
        primeList[0] = 2;

        // testedNumber is initialized at the first number greater than 2
        int testedNumber = 3;

        // The first empty element of primeList[] is primeList[1]
        int k = 1;

        // Only prime numbers are copied to the list of prime numbers
        while (primeList[index - 1] == 0) {

            if (Multiples.isPrime(testedNumber) == true) {
                primeList[k] = testedNumber;
                k++;
            }

            testedNumber ++;

        }

        return primeList[index - 1];
    }

    public static void main(String[] args) {

        // Problem question
        System.out.println("What is the 10 001st prime number?");

        // The problem gives 10001 as the researchedIndex
        int researchedIndex = 10001;
        int answer = searchPrimeOfIndex(researchedIndex);

        // Problem answer
        System.out.println("It is " + answer);
    }


}
