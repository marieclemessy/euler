/**
 * Gives the answer to the Euler problem #4
 * 
 * @author Marie Clemessy <marie.clemessy@gmail.com>
 * 
 */

package com.EulerAnswers;

public class Problem4 {

	public static void main(String[] args) {
		System.out.println("Find the largest palindrome made from the product of two 3-digit numbers.");
		
		
		// factor1 and factor2 are initialized at 999 so that they are 3-digit numbers
		int factor1 = 999;
		int factor2 = 999;
		int product;
		int highestPalindrome = 0;
		int highestFactor1 = 0;
		int highestFactor2 = 0;
		
		// factor1 needs to stay higher than 99 to be a 3 digit-number
		while (factor1 > 99) {
			
			// Re-initialize factor2 at 999 for the new loop
			factor2 = 999;
			
			// Factor1 needs to stay higher than 99 to be a 3 digit-number
			while (factor2 > 99) {
				
				product = factor1 * factor2;
				
				if (isPalindromic (product) == true && product > highestPalindrome) {
					
					// Saves product it is the highest palindromic number found up to this loop
					highestPalindrome = product;
					
					// Saves the 3-digit factors which give product if you multiply them together
					highestFactor1 = factor1;
					highestFactor2 = factor2;
					
					factor2 = factor2 - 1;
					
				} else {
					factor2 = factor2 - 1;
				}
			}
			factor1 = factor1 - 1;
			}
		
		System.out.println("The largest palindrome made from the product of two 3-digit numbers is : " + highestPalindrome);
		System.out.println("C'est le produit de : " + highestFactor1 + " et " + highestFactor2);

	}
	
	/**
	 * Verifies if the given number is a palindrome.
	 * A palindromic number reads the same both ways.
	 * 
	 * @return Returns true if testedNumber is a palindrome
	 */
	
	public static boolean isPalindromic (int number) {

		// Reverses the given number
		int reversedNumber = 0;
		int input = number;
		
		while (input != 0) {
			int last_digit = input % 10;
		    reversedNumber = reversedNumber * 10 + last_digit;
		    input = input / 10; 
		}
		
		// Verifies if the number reads the same both ways
		if (number == reversedNumber) {
			return true;
		} else {
			return false;
		}
		
	}

}
