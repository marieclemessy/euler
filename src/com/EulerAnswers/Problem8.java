package com.EulerAnswers;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Gives the answer to the Euler problem #8
 *
 * @author Marie Clemessy <marie.clemessy@gmail.com>
 * Created on 11/09/2014.
 */
public class Problem8 {

    /**
     * @param array The ArrayList that stores the digits to consider
     * @param numberAdjacentDigits Number of adjacent items to multiply
     * @return the highest product of adjacent numbers in a given ArrayList
     */

    public static long highestProduct (ArrayList<Integer> array, int numberAdjacentDigits) {
        int j = 0;
        long product = 1L;
        long highestProduct = 1L;

        // Tries every group of numberAdjactentDigits items
        while(j < array.size() - numberAdjacentDigits) {
            for (int i = 0; i < numberAdjacentDigits; i++) {
                product = product * array.get(j + i);
            }
            // Records the highest product found up to this iteration
            if (product > highestProduct) {
                highestProduct = product;
            }
            product = 1;
            j++;
        }

        return highestProduct;
    }

    /**
     *
     * @param file file in .txt format holding a very long number
     * @return an ArrayList with each digit of number as an item of the ArrayList
     */

    public static ArrayList<Integer> generateDigitsInArrayList(File file) {
        ArrayList digits = new ArrayList();
        try {

            Scanner scanner = new Scanner(file);

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                for (int i = 0; i < line.length(); i++) {
                    digits.add(Character.getNumericValue(line.charAt(i)));
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return digits;
    }


    public static void main(String[] args) {

        // Problem question
        System.out.println("Find the thirteen adjacent digits in the 1000-digit number that have the greatest product. What is the value of this product?");

        // The questions is looking for products of 13 adjacent digits
        int numberAdjacentDigits = 13;

        // Read list of Integer and stores in arrayList
        File file = new File("/Development/EulerProblems/Problem8_data.txt");
        ArrayList digits = new ArrayList();
        digits = generateDigitsInArrayList(file);

        // Problem answer
        long answer = highestProduct(digits, numberAdjacentDigits);
        System.out.println("It is " + answer);
    }

}
