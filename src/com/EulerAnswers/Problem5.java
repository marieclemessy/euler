/**
 * Gives the answer to the Euler problem #5
 * 
 * @author Marie Clemessy <marie.clemessy@gmail.com>
 * 
 */

package com.EulerAnswers;

public class Problem5 {

	public static void main(String[] args) {
		System.out.println("What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?");
		
		int number = 1;

		while (isEvenlyDivisible(number, 1, 20) == false) {
			number = number + 1;
		}
		
		System.out.println("It is " + number);
	
	}

	
	/**
	 * Verifies if the given number is a multiple of all numbers
	 * between two specified bounds. If so, the number is said to be
	 * evenly divisible by all numbers between the two specified bounds.
	 * 
	 * @param minMultiple is the lower bound
	 * @param mawMultilpe is the higher bound (inclusive)
	 * 
	 * @return Returns true if this.value is a multiple of all numbers
	 */
	public static boolean isEvenlyDivisible(int number, int minMultiple, int maxMultiple) {
		boolean evenlyDivisible = true;
		for (int i = minMultiple; i < maxMultiple + 1 ; i++)
			if (number % i != 0) {
				evenlyDivisible = false;
			}
		return evenlyDivisible;
	}

}
