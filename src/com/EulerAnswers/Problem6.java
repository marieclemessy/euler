/**
 * Gives the answer to the Euler problem #6
 * 
 * @author Marie Clemessy <marie.clemessy@gmail.com>
 * 
 */

package com.EulerAnswers;

public class Problem6 {

	public static void main(String[] args) {
		
		// Problem question
		System.out.println("Find the difference between the square of the sum of the first one hundred natural numbers and the sum of the squares.");
		
		// The problem gives 100 as an upper bound for the sums
		int bound = 100;
		int difference = squareOfSum(bound) - sumOfSquares(bound);
		
		// Problem answer
		System.out.println("It is " + difference);
	}
	
	/**
	 * Computes the sum of the squares of all positive integers
	 * up to the given upperBound.
	 * 
	 * @param upperBound is the given upper bound and is included in
	 * the computation
	 * 
	 * @return Returns the sum of the squares of all positive integers
	 * up to upperBound.
	 */
	
	public static int sumOfSquares(int upperBound) {
		int result = 0;
		
		for (int i = 1; i < upperBound + 1; i++) {
			result = result + i * i;
		}
		
		return result;
	}
	
	
	/**
	 * Computes the square of the sum of all positive integers
	 * up to the given upperBound.
	 * 
	 * @param upperBound is the given upper bound and is included in
	 * the computation
	 * 
	 * @return Returns the square of the sum of all positive integers
	 * up to upperBound.
	 */
	public static int squareOfSum(int upperBound) {
		int result;
		int sum = 0;
		
		// Sum of the divergent series of all positive integers
		sum = (upperBound * (upperBound + 1) ) / 2;
		
		result = sum * sum;
		
		return result;
	}

}
