# README #

This project provides programs to find the answers to the Euler problems.
Details regarding the Euler project can be found [here](http://projecteuler.net/).


***


### What is in this repository? ###

Package Name  | Details
------------- | -------------
src.com.EulerAnswers  | holds one file per Euler problem. Each of them has a class "main". Each file is numbered to match with its number on the [Euler problem list](http://projecteuler.net/problems).
src.com.functions  | holds the mathematical classes and functions used to answer the problems


### What is the version of this repository? ###

* The versions have yet to be numbered.


***


### How do I get set up? ###

* Go to the Navigation -> Source section of this page (on the left of your screen).
* Clone the repository to your computer.
* Choose a problem from the [Euler problem list](http://projecteuler.net/problems) you would like to find an answer to.
* Find the problemXX.java file which matches its number.
* Run the problemXX.java file you selected.


***


### Who do I talk to? ###

* Repository owner : Marie Clemessy.


***